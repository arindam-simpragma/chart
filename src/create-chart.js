var exporting = require('node-highcharts-exporting');
var fs = require('fs');

/*
* createChart(title,days,legends)
* title : chart title (string)
* xaxis : days
* yaxis : legends
* result: chart.png
*/
var drawChart = function(title,days,legends){
  // console.log(JSON.stringify(legends,null,1))
  exporting({
    data : {
        chart: {
            type: 'line',
            width:800,
            height: 600
           
        },
        xAxis: {
            categories: days,
            /*title:{
                text : "Days"
            },*/
            labels: {
                style: {
                    fontSize:'15px'
                }
            }
        },         
        legend: {
            title: {
                text: 'Metric'
            }
        },       
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true,
                    style: {
                        fontSize: '12px',
                        textOutline: 'none'
                    }
                },
            }
        },
        series: legends
    },
 
    options : {
        title : {text : title} ,
        "yAxis" : {
            title : {"text": "Load time (ms)" },
            labels: {
                style: {
                    fontSize:'15px'
                }
            }
        }
    }
 
} , function (err , data){
    // data had encode base64 , should be decode 
    if(err){
      console.log("Chart not created ",err)
    }else{
        fs.writeFile('chart.png', Buffer(data, 'base64'), function(err) {
            if (err) throw err;
            console.log('Written to chart.png');
            process.exit()
        });
    }
})
}

exports.drawChart = drawChart;
