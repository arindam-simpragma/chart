
var GoogleSpreadsheet = require('google-spreadsheet'); 
var chart = require('./create-chart.js'); // chart creation file.
var creds = require('../google-generated-creds.json'); // api authentication document
var num = 0;

var chartData = {};
chartData = {
  vars : {
    doc : new GoogleSpreadsheet('1SpuDW9vtZKA_iQ1HzBnSwcV9Ak6aDGgOdgdFYOycRTE'),
    sheet : [], // google sheets
    days : [], // x-axis Data
    loadTime : [], // load time for individual day (y-axis data)
    sheetTitle : '', // chart title
    legends : [], // matric name
    finalLegendsObj : [] // load time based on matric and day.
  },
  init : function(){
    self = this;
    self.auth();
  },
  auth : function(){ // authentication with google api
    self = this;   
      self.vars.doc.useServiceAccountAuth(creds, function(){
        self.getInfoAndWorksheets()
      });
  },
  getInfoAndWorksheets : function(){ // get all the sheets and push it to array
    self = this;
     self.vars.doc.getInfo(function(err, info) {
      self.vars.sheetTitle = info.title;
      for(var i in info.worksheets){
          self.vars.sheet.push(info.worksheets[i]);       
      }
      self.getLoadTimes();
    });
  },
  getLoadTimes : function(){ // get day names and and load time values
    self = this;
    console.log("--"+self.vars.sheet[num].title+"--")
      self.vars.days.push(self.vars.sheet[num].title);
      self.vars.sheet[num].getCells({
        'min-row': 5,
        'max-row': 12,
        'min-col': 7,
        'max-col': 7,
      },function(err, cells) {
        var lt = [];
        var obj = {};
        for(var i in cells){
          var cell = cells[i];  
          lt.push(cell.value);
        }        
        self.vars.loadTime.push(lt);
        if(num != self.vars.sheet.length - 1){
          num++;
          self.getLoadTimes();
        }else{
           self.getLegendLoadTime();
        }
      });
  },
  getLegendLoadTime : function(){ // get legend Names and make json as chart data
    self = this;
    self.vars.sheet[0].getCells({
        'min-row': 5,
        'max-row': 12,
        'min-col': 2,
        'max-col': 2,
      },function(err, cells) {
        for(var i in cells){
          self.vars.legends.push(cells[i].value); // legend name
        }  
        self.createLegendsObj(cells);       
      });
  },
  createLegendsObj : function(cells){
    self = this;
    let counter=0;
    while(counter < cells.length){
      let temp = [];
      for(var i=0;i<self.vars.loadTime.length ; i++){
          temp.push(parseInt(self.vars.loadTime[i][counter]))
      }
      var obj = {};
      obj.name = self.vars.legends[counter];
      obj.data = temp;
      self.vars.finalLegendsObj.push(obj);
      counter++;
    }
    console.log(JSON.stringify(self.vars.finalLegendsObj,null,1))
      // It's time to draw chart
      // sending data for drawing chart  
    chart.drawChart(self.vars.sheetTitle,self.vars.days,self.vars.finalLegendsObj)
  }
};

var newChart = new Object(chartData); // create a chartData instance 
newChart.init();
